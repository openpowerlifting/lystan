all: build

# Build the project into public/.
build: node_modules
	npm run webpack
	cp src/index.html public/

# Open the project in a new browser tab.
.PHONY: open
open: build
	xdg-open "public/index.html"

node_modules:
	npm install

.PHONY: clean
clean:
	rm -rf node_modules
	rm -rf public
