// vim: set ts=4 sts=4 sw=4 et:
//
// This file is part of Lystan, a powerlifting image generation tool.
// Copyright (C) 2020 The OpenPowerlifting Project.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import * as p5 from "p5";

import { Column, loadFromSource, RankingsData, scheduleRankingsDataLoad } from "./dataSource";

// Note that on High-DPI monitors, the actual canvas size will be twice this.
const INSTAGRAM_HEIGHT_PX = 1200;
const INSTAGRAM_WIDTH_PX = 1200;

let activeSketch: p5 | null = null;

let titleTextArea: HTMLTextAreaElement;

// Rankings data for display in the table, stored as a global.
let globalData: RankingsData = {
    rows: []
};

// Renders a number for display, truncating to one decimal place, hiding unnecessary zeros.
// Taken from OpenLifter's `src/logic/units.ts`.
function displayWeightOnePlace(weight: number): string {
  const locale = "en";

  // This matches the OpenPowerlifting WeightKg::as_lbs() conversion logic.
  // First, round to the hundredth place, stored as an integer.
  let rounded = Math.round(weight * 100);

  // If the fractional part is close to another tenth, add a correction.
  // This happens due to floating-point imprecision.
  if (rounded % 10 === 9) {
    // Add 0.01 (still stored as an integer).
    rounded += 1;
  }

  // Truncate to the tenth place, then convert back to normal floating-point.
  const truncated = Math.trunc(rounded / 10) / 10;

  return new Intl.NumberFormat(locale, { useGrouping: false, maximumFractionDigits: 1 }).format(truncated);
};

// Converts a Date to an ISO-8601 string (YYYY-MM-DD).
// Taken from OpenLifter's `src/logic/date.ts`.
function localDateToIso8601(d: Date): string {
  const year: number = d.getFullYear();
  const month: number = d.getMonth() + 1;
  const day: number = d.getDate();

  const yearStr = String(year);
  let monthStr = String(month);
  if (monthStr.length === 1) {
    monthStr = "0" + monthStr;
  }
  let dayStr = String(day);
  if (dayStr.length === 1) {
    dayStr = "0" + dayStr;
  }

  return yearStr + "-" + monthStr + "-" + dayStr;
}

// Gets the value for a radio group by name.
// Taken from the IPF Points Calculator.
function getRadioValue(name: string): string | undefined {
    const radios = document.getElementsByName(name);
    for (let i = 0; i < radios.length; ++i) {
        const radio = radios[i];
        if (radio instanceof HTMLInputElement) {
            if (radio.checked) { return radio.value; }
        }
    }
}

// Parameters passed to a sketch instance via closure.
type SketchContext = {
    width: number;  // Canvas width in effective pixels (High-DPI will have 2x).
    height: number;  // Canvas height in effective pixels (High-DPI will have 2x).

    // Background fill colors.
    titleBackground: string;  // Color used for the title at the top.
    headerBackground: string;  // Color used for the accent table header.
    oddRowBackground: string;  // Color used for odd rows (the rows are zero-indexed).
    evenRowBackground: string;  // Color used for even rows.
    footerBackground: string,  // Color used for the informational footer.

    // Foreground text colors.
    titleTextColor: string;
    headerTextColor: string;
    oddRowTextColor: string;
    evenRowTextColor: string;
    footerTextColor: string;
};

// Default context for OpenPowerlifting.org-style images.
const CONTEXT_OPL: Readonly<SketchContext> = {
    width: INSTAGRAM_WIDTH_PX,
    height: INSTAGRAM_HEIGHT_PX,

    titleBackground: "#F3F3F3",
    headerBackground: "#BF262B",
    oddRowBackground: "#272727",
    evenRowBackground: "#333333",
    footerBackground: "#000",

    titleTextColor: "#4A4A50",
    headerTextColor: "#EDF2F4",
    oddRowTextColor: "#EDF2F4",
    evenRowTextColor: "#EDF2F4",
    footerTextColor: "#EDF2F4",
};

// Context for OpenIPF.org-style images.
const CONTEXT_IPF: Readonly<SketchContext> = {
    width: INSTAGRAM_WIDTH_PX,
    height: INSTAGRAM_HEIGHT_PX,

    titleBackground: "#303030",
    headerBackground: "#FDB93E",
    oddRowBackground: "#F4F4F4",
    evenRowBackground: "#E8E6E7",
    footerBackground: "#E8E6E7",

    titleTextColor: "#F4F4F4",
    headerTextColor: "#120D0B",
    oddRowTextColor: "#120D0B",
    evenRowTextColor: "#120D0B",
    footerTextColor: "#303030",
};

// Global color context.
let cx: Readonly<SketchContext> = CONTEXT_OPL;

type VerticalCoordinates = {
    y: number;  // Top-left corner of row, in pixels.
    height: number;  // Height of row, in pixels.
};

type RowCoordinates = {
    title: VerticalCoordinates;  // Spans two effective rows, plus extra pixels from rounding.
    header: VerticalCoordinates;  // Contains column headers.
    rows: Array<VerticalCoordinates>;  // Information from each row, sequentially from the top.
    footer: VerticalCoordinates;  // Informational footer about the project.
};

// Each image is partitioned vertically into a grid of 14 equal-sized elements.
// This function figures out the y (top-left) and height parameters for each row.
function partitionVertically(height: number): RowCoordinates {
    // Figure out the unit height for each row.
    // Extra pixels from rounding will go into the header.
    const unit_height = Math.floor(height / 14);
    const extra_pixels = height - (unit_height * 14);

    const title = { y: 0, height: unit_height * 2 + extra_pixels };
    const header = { y: title.y + title.height, height: unit_height };
    const row1 = { y: header.y + header.height, height: unit_height };
    const row2 = { y: row1.y + row1.height, height: unit_height };
    const row3 = { y: row2.y + row2.height, height: unit_height };
    const row4 = { y: row3.y + row3.height, height: unit_height };
    const row5 = { y: row4.y + row4.height, height: unit_height };
    const row6 = { y: row5.y + row5.height, height: unit_height };
    const row7 = { y: row6.y + row6.height, height: unit_height };
    const row8 = { y: row7.y + row7.height, height: unit_height };
    const row9 = { y: row8.y + row8.height, height: unit_height };
    const row10 = { y: row9.y + row9.height, height: unit_height };
    const footer = { y: row10.y + row10.height, height: unit_height };

    return {
        title: title,
        header: header,
        rows: [row1, row2, row3, row4, row5, row6, row7, row8, row9, row10],
        footer: footer,
    };
}

type HorizontalCoordinates = {
    x: number;  // Top-left corner of column, in pixels.
    width: number;  // Width of column, in pixels.
};

type ColCoordinates = {
    rank: HorizontalCoordinates;
    name: HorizontalCoordinates;
    kilos: HorizontalCoordinates;
    pounds: HorizontalCoordinates;
};

// Partition the width into disjoint columns.
function partitionHorizontally(width: number): ColCoordinates {
    // Insert spacing to the left of each column and to the right of the last column.
    const num_columns = 4;
    const padding = Math.floor(width / 60);
    const total_padding = padding * (num_columns + 1);

    const unit_width = Math.floor((width - total_padding) / 6);
    const rank_width = Math.floor(unit_width / 2);

    const extra_pixels = width - total_padding - (unit_width * 6) + (unit_width - rank_width);

    const rank = { x: padding, width: rank_width };
    const name = { x: rank.x + rank.width + padding, width: unit_width * 3 + extra_pixels };
    const kilos = { x: name.x + name.width + padding, width: unit_width };
    const pounds = { x: kilos.x + kilos.width + padding, width: unit_width };

    return {
        rank: rank,
        name: name,
        pounds: pounds,
        kilos: kilos,
    };
}

// Colors in the background of the rows.
function colorRows(p: p5, rowCoords: Readonly<RowCoordinates>) {
    // Draw the title row.
    p.fill(cx.titleBackground);
    p.rect(0, rowCoords.title.y, cx.width, rowCoords.title.height);

    // Draw the header row.
    p.fill(cx.headerBackground);
    p.rect(0, rowCoords.header.y, cx.width, rowCoords.header.height);

    // Draw the rankings rows.
    for (let i = 0; i < rowCoords.rows.length; ++i) {
        p.fill(i % 2 == 0 ? cx.evenRowBackground : cx.oddRowBackground);
        p.rect(0, rowCoords.rows[i].y, cx.width, rowCoords.rows[i].height);
    }

    // Draw the footer row.
    p.fill(cx.footerBackground);
    p.rect(0, rowCoords.footer.y, cx.width, rowCoords.footer.height);
}

// Writes the title text.
function writeTitleText(p: p5, rowCoords: Readonly<RowCoordinates>) {
    // This is handled differently depending on whether there are 1 or 2 lines.
    const title: string = titleTextArea.value;

    // Give a bit of vertical padding, otherwise the text is too close to the top.
    const paddedY = rowCoords.title.y + rowCoords.title.height / 15;

    p.fill(cx.titleTextColor);
    p.textStyle(p.BOLD);
    p.textSize(Math.floor(rowCoords.title.height / 2.5));  // In pixels.

    if (title.includes('\n')) {
        // Two lines. Render them separately, centering both in their own boxes.
        const [first, second] = title.split('\n');
        const textRowHeight = (rowCoords.title.height - paddedY) / 2;

        p.textAlign(p.CENTER, p.CENTER);
        p.text(first, 0, paddedY, cx.width, textRowHeight);
        p.text(second, 0, paddedY + textRowHeight, cx.width, textRowHeight);
    } else {
        // One line, easy case.
        p.textAlign(p.CENTER, p.CENTER);
        p.text(title, 0, paddedY, cx.width, rowCoords.title.height - paddedY);
    }
}

// Writes the column header text.
function writeHeaderText(p: p5, rows: RowCoordinates, cols: ColCoordinates) {
    p.fill(cx.headerTextColor);
    p.textStyle(p.BOLD);
    p.textSize(Math.floor(rows.header.height / 2));

    p.textAlign(p.RIGHT, p.CENTER);
    p.text("#", cols.rank.x, rows.header.y, cols.rank.width, rows.header.height);

    p.textAlign(p.LEFT, p.CENTER);
    p.text("LIFTER", cols.name.x, rows.header.y, cols.name.width, rows.header.height);
    p.text("KG", cols.kilos.x, rows.header.y, cols.kilos.width, rows.header.height);
    p.text("LBS", cols.pounds.x, rows.header.y, cols.pounds.width, rows.header.height);
}

// Writes the data for the ith data row (0-indexed).
function writeRowText(
    p: p5,
    idx: number,
    data: Array<string | number | null>,
    rows: RowCoordinates,
    cols: ColCoordinates
) {
    // Get the vertical positioning of this row.
    const y = rows.rows[idx].y;
    const height = rows.rows[idx].height;

    p.fill(idx % 2 === 0 ? cx.evenRowTextColor : cx.oddRowTextColor);
    p.textStyle(p.BOLD);
    p.textSize(Math.floor(height / 2.2));

    const rank: string = String(Number(data[Column.SortedIndex]) + 1);

    // Get a name without disambiguations.
    let name: string = String(data[Column.Name]);
    if (name.includes('#')) {
        name = name.substr(0, name.indexOf('#') - 1);
    }

    const kg_num: number = Number(String(data[Column.Total]).replace(",", "."));
    const kilos: string = displayWeightOnePlace(kg_num);
    const pounds: string = displayWeightOnePlace(kg_num * 2.20462262);

    p.textAlign(p.RIGHT, p.CENTER);
    p.text(rank, cols.rank.x, y, cols.rank.width, height);

    p.textAlign(p.LEFT, p.CENTER);
    p.text(name, cols.name.x, y, cols.name.width, height);
    p.text(kilos, cols.kilos.x, y, cols.kilos.width, height);
    p.text(pounds, cols.pounds.x, y, cols.pounds.width, height);
}

// Writes the footer text.
function writeFooterText(p: p5, rows: RowCoordinates) {
    p.fill(cx.footerTextColor);
    p.textStyle(p.NORMAL);
    p.textSize(Math.floor(rows.footer.height / 3));
    p.textAlign(p.CENTER, p.CENTER);

    const date: string = localDateToIso8601(new Date());
    const str: string = `As of ${date}. New and old meets are being added every day! If you see an error or omission please contact us: issues@openpowerlifting.org.`

    p.text(str, 0, rows.footer.y, cx.width, rows.footer.height);
}

function sketch(p: p5) {
    p.setup = () => {
        p.createCanvas(cx.width, cx.height);
    }

    p.draw = () => {
        p.background('red');  // Set the background to red.
        p.noStroke();  // Disable drawing borders around each shape.

        // Set the global font.
        p.textFont("Droid Sans");

        const rowCoords = partitionVertically(cx.height);
        const colCoords = partitionHorizontally(cx.width);

        colorRows(p, rowCoords);  // Draw row backgrounds.
        writeTitleText(p, rowCoords);  // Draw title text.
        writeHeaderText(p, rowCoords, colCoords);  // Draw column headers.

        // Write rows.
        for (let i = 0; i < Math.min(globalData.rows.length, 10); ++i) {
            const rowData = globalData.rows[i];
            writeRowText(p, i, rowData, rowCoords, colCoords);
        }

        writeFooterText(p, rowCoords);  // Draw footer text.
    }
};

// Called by scheduleRankingsDataLoad() if the AJAX call succeeded and parsed plausibly.
// FIXME: This can't be used until we have a real data API due to CORS.
function dataCallback(url: string, data: RankingsData) {
    // Overwrite the global with the new stuff.
    globalData = data;
}

function redraw() {
    // Remove any <canvas> elements currently rendered.
    if (activeSketch !== null) {
        activeSketch.remove();
        activeSketch = null;
    }

    // Make a new <canvas>.
    const canvasContainer = document.getElementById("canvas-container");
    if (canvasContainer instanceof HTMLElement) {
        activeSketch = new p5(sketch, canvasContainer);
    }
}

function setTheme() {
    switch (getRadioValue("theme")) {
        case "opl": cx = CONTEXT_OPL; break;
        case "ipf": cx = CONTEXT_IPF; break;
        default: break;
    }
}

function initializeEventListeners() {
    titleTextArea = document.getElementById("titleTextArea") as HTMLTextAreaElement;

    // Theme selectors.
    const oplTheme = document.getElementById("oplTheme") as HTMLInputElement;
    const ipfTheme = document.getElementById("ipfTheme") as HTMLInputElement;
    oplTheme.addEventListener("change", e => { setTheme() }, false );
    ipfTheme.addEventListener("change", e => { setTheme() }, false );

    /* TODO
    const urlButton = document.getElementById("urlButton") as HTMLButtonElement;
    urlButton.addEventListener("click", e => scheduleRankingsDataLoad(dataCallback), false);
    */

    // This is a temporary thing where users copy/paste "View Source", because
    // OpenPowerlifting doesn't have a data API yet.
    const source = document.getElementById("sourceTextArea") as HTMLTextAreaElement;
    const sourceButton = document.getElementById("sourceButton") as HTMLButtonElement;
    sourceButton.addEventListener("click", e => {
        const data = loadFromSource(source.value);
        dataCallback("https://www.openpowerlifting.org", data);
    }, false);
}

function main() {
    initializeEventListeners();
    redraw();
}

document.addEventListener("DOMContentLoaded", main);
