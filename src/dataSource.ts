// vim: set ts=4 sts=4 sw=4 et:
//
// This file is part of Lystan, a powerlifting image generation tool.
// Copyright (C) 2020 The OpenPowerlifting Project.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Gets rankings information from OpenPowerlifting.

// Column mapping for the server rankings JSON.
// Taken from OpenPowerlifting's remotecache.ts.
// This is very fragile, but we haven't built the stable API yet.
export const enum Column {
    SortedIndex,
    Rank,
    Name,
    Username,
    Instagram,
    Vkontakte,
    Color,
    Flair,
    Federation,
    Date,
    Country,
    State,
    Path,
    Sex,
    Equipment,
    Age,
    Division,
    Bodyweight,
    WeightClass,
    Squat,
    Bench,
    Deadlift,
    Total,
    Points,
};

export type RankingsData = {
    // Missing items are null; rank is a number; everything else is a string.
    rows: Array<Array<string | number | null>>;
};

// Hack to get rankings data out of OpenPowerlifting HTML that a user copy/pasted.
export function loadFromSource(html: string): RankingsData {
    let rows = [];

    const lines = html.split('\n');
    for (let i = 0; i < lines.length; ++i) {
        // The rankings data is initialized in a variable "initial_data" on one line.
        const line = lines[i];
        if (line.includes("const initial_data = {")) {
            const start = line.indexOf('{');

            // Omit the trailing semicolon, or parsing fails.
            const length = line.length - start - 1;

            const jsonstr = line.substr(start, length);
            const json = JSON.parse(jsonstr);
            
            rows = json.rows;
        }
    }

    return {
        rows: rows,
    };
}

// FIXME: This can't be used until we get a real data API due to CORS.
export function scheduleRankingsDataLoad(callback: (url: string, d: RankingsData) => void) {
    let urlElement = document.getElementById("urlText") as HTMLInputElement;
    let url = urlElement.value;

    let handle = new XMLHttpRequest();
    handle.open("GET", url)
    handle.responseType = "text";

    handle.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            const html = this.responseText;

            const data = {
                rows: []
            };

            callback(url, data);
        }
    };

    handle.send();
}
